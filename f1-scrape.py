
import csv
from datetime import datetime
from formula1 import *
import time

year = 2018

# get the drivers leader board
wdc = get_wdc_results(year)
print(wdc)
time.sleep(0.3)

# get lewis hamilton's results for every race, this year
ham = get_driver_results("HAM", year, wdc)
print(ham)
time.sleep(0.3)

# get vettel's results for every race this year
seb = get_driver_results("Sebas", year)  # slower, fetches wdc_results first (seb is slower!)
print(seb)
time.sleep(0.3)

# get the teams leader board
wcc = get_wcc_results(year)
print(wcc)
time.sleep(0.3)

# get mercedes team results for every race this year
merc = get_team_results("Merc", year, wdc)
print(merc)
time.sleep(0.3)

# get ferrari team results
sf = get_team_results("Fer", year)
print(sf)
time.sleep(0.3)

# get the fastest laps
dhlfl = get_fastest_lap_results(year)
print(dhlfl)
