"""
Plot Formula 1 fastest laps, by driver.
"""
from formula1 import *
import time
import matplotlib.pyplot as plot
from operator import itemgetter
kimi_laps = [] # dictionary
kimi_years = [] # array
kimi_fasts = []
total = 0
for year in range(2001,2020):
    print("fetching fastest laps from {0} ...".format(year))
    fl = get_fastest_lap_results(year)
    print("found {0} fastest laps".format(len(fl)))
    count = 0
    for lap in fl:
        if lap['driver'].find('RAI') > 0 :
            count = count + 1
            total = total + 1
            kimi_laps.append([year, lap['race'], lap['lap_time']])
            print("{0} - {1:<14} : {2:<27} ({3})  [{4} total]".format(year, lap['race'], lap['driver'], lap['lap_time'], total))
    print("{0}: Kimi got {1} fastest laps ({2} total)".format(year, count, total))
    kimi_years.append(year)
    kimi_fasts.append(count)
    # print("sleep...")
    time.sleep(0.5) # be nice to the server
    print()

# plot the distribution
plot.bar(kimi_years,kimi_fasts,color=(0.6,0.1,0.2,1.0), edgecolor='black')
plot.xticks(kimi_years)
plot.grid(True)
plot.xlabel('F1 Year')
plot.ylabel('Number of Fastest Laps')
plot.title("Kimi's Fastest Laps")

plot.grid(True)
plot.show()


