"""
FORMULA ONE web site data fetch utilities

This Python module contains utilities for scraping data from the Formula1.com web site.

Copyright 2019, Matthew Grund.
"""

from urllib.request import urlopen
from bs4 import BeautifulSoup
import time


def get_wdc_results(year=2018):
    """
    Fetches Formula 1 World Drivers Championship (WDC) standings for a given year
    from www.formula1.com
    :param year: the Formula 1 race year (1950 to present race year)
    :return: a list of driver standings (dictionaries), one for each driver in the WDC results list.
    """
    # Fetch the WDC drivers results from F1.com URL
    drivers_url = "https://www.formula1.com/en/results.html/{0}/drivers.html".format(year)
    drivers_html = urlopen(drivers_url)
    drivers_soup = BeautifulSoup(drivers_html, 'html.parser')
    drivers_tags = drivers_soup.find_all("td")
    # drivers_links = drivers_soup.select("td > a")
    # print(drivers_links)

    # parse the results table data
    drivers = []
    for field,tag in enumerate(drivers_tags):
        col = (field % 7)
        if col == 0:
            row={}
        if col == 1:
            row['place'] = int(tag.get_text())
        if col == 2:
            row['driver'] = tag.get_text().replace("\n"," ").strip()
            row['driver_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 3:
            row['nationality'] = tag.get_text()
        if col == 4:
            row['team'] = tag.get_text().replace("\n"," ").strip()
            row['team_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 5:
            row['points'] = int(tag.get_text())
        if col == 6:
            drivers.append(row)
    return drivers


def get_driver_results(driver_name, year=2018, wdc_results=[]):
    """
    Fetches Formula 1 Driver results for a given year from www.formula1.com
    :param driver_name: A string to match to the driver name, as it appears in the wdc_results.
            e.g. "Lewis Hamilton HAM". Uses str.find, so substrings work. Only the first matching result is
            returned.
    :param year: the Formula 1 race year (1950 to present race year)
    :param wdc_results: for speed with multiple driver lookups, feed in the WDC results for the year
    :return: a list of driver race results dictionaries, one for each race in the given year.
    """
    # get wdc_results if we dont have them already (need for link to driver results)
    if len(wdc_results) == 0 :
        wdc_results = get_wdc_results(year)
        time.sleep(0.300) # throttle our hits on the web site

    # now look for the link associated with the driver
    driver_link=""
    for driver in wdc_results:
        #print("Match: '{0}' in '{1}'?".format(driver_name,driver['driver']))
        if driver['driver'].find(driver_name) != -1:
            driver_link = driver['driver_link']
            break
    if len(driver_link) == 0:
        print("driver {0} not found".format(driver_name))
        return

    driver_html = urlopen(driver_link)
    driver_soup = BeautifulSoup(driver_html, 'html.parser')
    driver_tags = driver_soup.find_all("td")

    races = []
    for field,tag in enumerate(driver_tags):
        #print("==={0}===".format(field))
        #print(tag)
        col = (field % 7)
        if col == 0:
            row={}
        if col == 1:
            row['race'] = tag.get_text().replace("\n", " ").strip()
            row['race_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 2:
            row['date'] = tag.get_text().replace("\n", " ").strip()
        if col == 3:
            row['team'] = tag.get_text().replace("\n", " ").strip()
            row['team_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 4:
            row['place'] = tag.get_text()
        if col == 5:
            row['points'] = int(tag.get_text())
        if col == 6:
            races.append(row)
    return races


def get_wcc_results(year=2018):
    """
    Fetches the Formula 1 World Constructor Championship standings for the given year from www.formula1.com
    :param year: the Formula 1 race year (1950 to present race year)
    :return: a list of constructor standings results (dictionary), one per Constructor (team).
    """
    # Fetch the WCC team results from F1.com URL
    #            https://www.formula1.com/en/results.html/2018/team.html
    teams_url = "https://www.formula1.com/en/results.html/{0}/team.html".format(year)
    teams_html = urlopen(teams_url)
    teams_soup = BeautifulSoup(teams_html, 'html.parser')
    teams_tags = teams_soup.find_all("td")

    # parse the results table data
    teams = []
    for field,tag in enumerate(teams_tags):
        col = (field % 5)
        # print("=== {0} ===".format(col))
        # print(tag)
        if col == 0:
            row={}
        if col == 1:
            row['place'] = tag.get_text()
        if col == 2:
            row['team'] = tag.get_text().replace("\n", " ").strip()
            row['team_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 3:
            row['points'] = int(tag.get_text())
        if col == 4:
            teams.append(row)
    return(teams)


def get_team_results(team_name, year=2018, wdc_or_wcc_results=[]):
    """
    Fetches the Formula 1 team's race results for the given year from www.formula1.com
    :param team_name:  string to match to the team name, as it appears in the wcc_results.
            e.g. "Red Bull Racing TAG Heuer". Uses str.find, so substrings work. Only the first matching result is
            returned.
    :param year: the Formula 1 race year (1950 to present race year)
    :param wdc_or_wcc_results: for speed with multiple driver lookups, feed in the WDC or WCC results for the year
    :return: a list of team race results (dictionary), one for each race that year.
    """
    # get wdc_results if we dont have them already (need for link to driver results)
    if len(wdc_or_wcc_results) == 0 :
        wdc_or_wcc_results = get_wcc_results(year) # wcc is half as big
        time.sleep(0.300) # throttle our hits on the web site

    # now look for the link associated with the team
    team_link=""
    for team in wdc_or_wcc_results:
        # print("Match: '{0}' in '{1}'?".format(team_name, team['team']))
        if team['team'].find(team_name) != -1:
            team_link = team['team_link']
            # print("Found team matching '{0}' => {1}".format(team_name, team['team']))
            break
    if len(team_link) == 0:
        print("team '{0}' not found".format(team_name))
        return

    team_html = urlopen(team_link)
    team_soup = BeautifulSoup(team_html, 'html.parser')
    team_tags = team_soup.find_all("td")

    races = []
    for field, tag in enumerate(team_tags):
        col = (field % 5)
        # print("==={0}===".format(col))
        # print(tag)
        if col == 0:
            row={}
        if col == 1:
            row['race'] = tag.get_text().replace("\n", " ").strip()
            row['race_link'] = "https://www.formula1.com" + tag.a['href']
        if col == 2:
            row['date'] = tag.get_text().replace("\n", " ").strip()
        if col == 3:
            row['points'] = int(tag.get_text())
        if col == 4:
            races.append(row)
    return races


def get_fastest_lap_results(year=2018):
        """
        Fetch the fastest lap results for the given year from formula1.com
        :param year: the Formula 1 race year (1950 to present race year)
        :return: a list of fastest lap results (dictionary), one per race, that year.
        """
        # Fetch the lap results from F1.com URL
        #            https://www.formula1.com/en/results.html/2009/fastest-laps.html
        laps_url = "https://www.formula1.com/en/results.html/{0}/fastest-laps.html".format(year)
        laps_html = urlopen(laps_url)
        laps_soup = BeautifulSoup(laps_html, 'html.parser')
        laps_tags = laps_soup.find_all("td")

        # parse the results table data
        laps = []
        for field, tag in enumerate(laps_tags):
            col = (field % 6)
            # print("=== {0} ===".format(col))
            # print(tag)
            if col == 0:
                row = {}
            if col == 1:
                row['race'] = tag.get_text()
            if col == 2:
                row['driver'] = tag.get_text().replace("\n", " ").strip()
            if col == 3:
                row['team'] = tag.get_text().replace("\n", " ").strip()
            if col == 4:
                row['lap_time'] = tag.get_text().replace("\n", " ").strip()
            if col == 5:
                laps.append(row)
        return (laps)
