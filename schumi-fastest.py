"""
Plot Formula 1 fastest laps, by driver.
"""
from formula1 import *
import time
from operator import itemgetter
schumi_laps = [] # dictionary
total = 0
for year in range(1991,2013):
    print("fetching fastest laps from {0} ...".format(year))
    fl = get_fastest_lap_results(year)
    print("found {0} fastest laps".format(len(fl)))
    count = 0
    for lap in fl:
        if lap['driver'].find('MSC') > 0 :
            count = count + 1
            total = total + 1
            schumi_laps.append([year, lap['race'],lap['lap_time']])
            print("{0} - {1:<14} : {2:<27} ({3})  [{4} total]".format(year, lap['race'], lap['driver'], lap['lap_time'], total))
    print("{0}: MSC got {1} fastest laps ({2} total)".format(year, count, total))

    # print("sleep...")
    time.sleep(0.5) # be nice to the server
    print()




