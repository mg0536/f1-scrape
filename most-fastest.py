"""
Plot Formula 1 fastest laps, by driver.
"""
from formula1 import *
import time
from operator import itemgetter
driver_laps = {} # dictionary
msc_count = 0 ;
for year in range(1950,2019):
    print("fetching fastest laps from {0} ...".format(year))
    fl = get_fastest_lap_results(year)
    print("found {0} fastest laps".format(len(fl)))
    for lap in fl:
        if lap['driver'].find('MSC') > 0:
            msc_count = msc_count + 1
            print("{0} - {1:<17} : {2:<29} ({3}) [{4} total]".format(year, lap['race'], lap['driver'], lap['lap_time'], msc_count))
        else:
            print("{0} - {1:<17} : {2:<29} ({3})".format(year, lap['race'], lap['driver'], lap['lap_time']))

        if lap['driver'] in driver_laps:
            count = driver_laps[lap['driver']]
            count = count + 1
            driver_laps[lap['driver']] = count
        else:
            driver_laps[lap['driver']] = 1
    print("sleep...")
    time.sleep(0.5) # be nice to the server
    print()

drivers = []

# reshape dict to a list
for driver in driver_laps:
    drivers.append([driver, driver_laps[driver]])
    # print("[{0}]\t{1}".format(driver_laps[driver],driver))

# drivers.sort()
# for driver in drivers:
#    print(driver)
# print("=================")
d2=sorted(drivers,key=itemgetter(1),reverse=True)
iter = 0
for name, laps in d2:
    iter = iter + 1
    print("{0:>3}. {1:<27}\t ({2})".format(iter, name, laps))


